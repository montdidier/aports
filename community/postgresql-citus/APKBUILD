# Contributor: Michael Pirogov <vbnet.ru@gmail.com>
# Maintainer: Michael Pirogov <vbnet.ru@gmail.com>
pkgname=postgresql-citus
_projname=citus
pkgver=11.1.3
pkgrel=0
pkgdesc="Scalable PostgreSQL for multi-tenant and real-time analytics workloads"
url="https://github.com/citusdata/citus"
arch="all"
license="AGPL-3.0-only"
makedepends="
	autoconf
	icu-dev
	libxml2-dev
	lz4-dev
	postgresql-dev
	zstd-dev
	"
install="$pkgname.post-install"
subpackages="
	$pkgname-bitcode
	$pkgname-dev
	"
source="https://github.com/citusdata/citus/archive/v$pkgver/$_projname-$pkgver.tar.gz"
builddir="$srcdir/$_projname-$pkgver"
options="!check" # tests requires running postgresql cluster

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--without-libcurl
	make
}

package() {
	depends="postgresql$(pg_config --major-version)"

	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

bitcode() {
	pkgdesc="$pkgdesc (bitcode for JIT)"
	depends="$pkgname=$pkgver-r$pkgrel"
	install_if="postgresql$(pg_config --major-version)-jit $pkgname=$pkgver-r$pkgrel"

	amove usr/lib/postgresql*/bitcode
}

sha512sums="
954e11715f516ffc668e14426d8f1d010c5c4e6f1d206ce4f3122f5877206808cd7a180d97c8e70624840729310814bf0e35244b3f7782834b319852003734bc  citus-11.1.3.tar.gz
"
